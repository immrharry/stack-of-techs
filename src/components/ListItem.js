import React from "react";
import {
  View,
  Text,
  StyleSheet,
  TouchableWithoutFeedback,
  Platform,
  UIManager,
  LayoutAnimation
} from "react-native";
import { CardSection, Card } from "./common";
import * as actions from "../actions";
import { connect } from "react-redux";

if (
  Platform.OS === "android" &&
  UIManager.setLayoutAnimationEnabledExperimental
) {
  UIManager.setLayoutAnimationEnabledExperimental(true);
}

const ListItem = props => {
  const { id, title } = props.library;

  const renderDescription = () => {
    const { library, selectedLibraryId } = props;

    if (library.id === selectedLibraryId) {
      return (
        <CardSection>
          <Text style={styles.textStyleInDesc}>{library.description}</Text>
        </CardSection>
      );
    }
  };
  return (
    <Card>
      <TouchableWithoutFeedback
        onPress={() => {
          props.selectLibrary(id);
          LayoutAnimation.configureNext(LayoutAnimation.Presets.spring);
        }}
      >
        <View>
          <CardSection>
            <Text style={styles.textStyle}>{title}</Text>
          </CardSection>
          {renderDescription()}
        </View>
      </TouchableWithoutFeedback>
    </Card>
  );
};

const styles = StyleSheet.create({
  textStyle: {
    fontSize: 18,
    paddingLeft: 15
  },
  textStyleInDesc: {
    flex: 1,
    paddingHorizontal: 5
  }
});

const mapStateToProps = state => {
  return { selectedLibraryId: state.selectedLibraryId };
};

export default connect(mapStateToProps, actions)(ListItem);
