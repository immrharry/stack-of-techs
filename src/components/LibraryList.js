import React from "react";
import { connect } from "react-redux";
import { View, FlatList } from "react-native";
import ListItem from "./ListItem";

const LibraryList = ({ libraries }) => {
  const renderItem = library => {
    return <ListItem library={library} />;
  };

  return (
    <FlatList
      data={libraries}
      keyExtractor={library => library.id.toString()}
      renderItem={({ item }) => renderItem(item)}
    />
  );
};

const mapStateToProps = state => {
  return { libraries: state.libraries };
};

export default connect(mapStateToProps)(LibraryList);
